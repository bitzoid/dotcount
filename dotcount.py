#!/usr/bin/python3

import cv2
import operator

def curry2(f):
  return lambda x: lambda y: f(x,y)

def curry3(f):
  return lambda x: lambda y: lambda z: f(x,y,z)

def compose(f,g):
  return lambda x: f(g(x))

def clamp(low, high, x):
  return max(low,min(high,x))

ccompose = curry2(compose)
cadd = curry2(operator.add)
cmul = curry2(operator.mul)
cclamp = curry3(clamp)


def dot_count(img,rgb,radius,error,rgb_error=4,blur=3,verify=None):
  if type(img) is str:
    img = cv2.imread(img)

  if blur > 0:
    blur = cv2.medianBlur(img, blur)
  else:
    blur = img

  colorclamp = cclamp(0)(255)

  bgr = tuple(reversed(rgb))

  lower = tuple(map(compose(colorclamp,cadd(-rgb_error)),bgr))
  upper = tuple(map(compose(colorclamp,cadd(+rgb_error)),bgr))

  mask = cv2.inRange(blur,lower,upper)

  hc = cv2.HoughCircles(mask,cv2.HOUGH_GRADIENT,1,int(radius*0.75),param1=int(radius),param2=int(radius/2), minRadius=int(radius/error),maxRadius=int(radius*error))
  circles = []
  if hc is not None:
    circles = hc[0]

  if verify is not None:
    assert(type(verify) is str)
    output = img.copy()
    for (x, y, r) in map(curry2(map)(int),circles):
      cv2.circle(output, (int(x), int(y)), int(r), (255, 255, 255), -1)
      cv2.circle(output, (int(x), int(y)), int(r), (0, 0, 0), 2)
      cv2.line(output, (int(x), int(y-r)), (int(x), int(y+r)), rgb, 1)
      cv2.line(output, (int(x-r), int(y)), (int(x+r), int(y)), rgb, 1)
    cv2.imwrite(verify,output)

  return len(circles)

def show_help(prog):
  print(prog)
  print('    Count dots in images.')
  print()
  print('Flags')
  print('    --out         \tCreate out_input.jpg for each input.jpg.')
  print('    --no-out      \tDeactivate --out. [default]')
  print('    --fmt=STR     \tFormat for output files. The % character will be replaced with the input filename. [default=out_%]')
  print('    --pref        \tPrint each input.jpg filename before printing the dot count. [default]')
  print('    --no-pref     \tDeactivate --pref.')
  print('    --colour=STR  \tParse STR as a colour to search for. [default=red]')
  print('    --color=STR   \tAlias for --colour.')
  print('    --radius=UINT \tRadius of dots. [default=20]')
  print('    --error=FLOAT \tAllowed error of radius. This means dots of size radius/error up to radius*error will be found. [default=1.5]')
  print('    --total       \tPrint total count for all files instead of individual counts. (note: pass --total after the last file to get both)\n')
  print('    --no-total    \tPrint individual counts for files instead of one total count. [default]\n')
  print()
  print('Example')
  print('    '+prog+' --no-pref --radius=10 example.jpg')
  print('    Counts red dots with radius 10 in example.jpg (which is public domain and should come packaged with this tool).')
  print('    '+prog+" --out --pref --colour='#00FF00' greendots.jpg --colour='blue' bluedots.jpg bluedots2.jpg")
  print('    Counts green dots in greendots.jpg and blue dots in bluedots.jpg and bluedots2.jpg')
  print('    '+prog+" --out --colour=red --fmt=red.jpg input.jpg --colour=blue --fmt=blue.jpg input.jpg")
  print('    Counts red and blue dots in input.jpg. Output for red count is in red.jpg, output for blue is in blue.jpg')
  print()
  print('Licensed under GPLv3.')

if __name__ == '__main__':
  import sys
  import colour

  radius = 20
  error = 1.5
  out = False
  pref = True
  fmt = 'out_%'
  show_total = False
  total = 0

  c = colour.Color('red')

  for arg in sys.argv[1:]:
    if arg == '--help':
      show_help(sys.argv[0])
      exit(1)

  for arg in sys.argv[1:]:
    if False:
      pass
    elif arg == '--out':
      out = True
    elif arg == '--no-out':
      out = False
    elif arg == '--pref':
      pref = True
    elif arg == '--no-pref':
      pref = False
    elif arg == '--total':
      show_total = True
    elif arg == '--no-total':
      show_total = False
    elif arg.startswith('--fmt='):
      fmt = arg[6:]
    elif arg.startswith('--color='):
      c = colour.Color(arg[8:])
    elif arg.startswith('--colour='):
      c = colour.Color(arg[9:])
    elif arg.startswith('--radius='):
      radius = int(arg[9:])
    elif arg.startswith('--error='):
      error = float(arg[8:])
    else:
      onm = None
      if out:
        onm = fmt.replace('%',arg)
      count = dot_count(arg,tuple(map(cmul(255),c.rgb)),radius,error,rgb_error=16,blur=5,verify=onm)
      total += count
      if not show_total:
        if pref:
          print(arg+'\t'+str(count))
        else:
          print(count)

  if show_total:
    if pref:
      print('Total\t'+str(total))
    else:
      print(total)




